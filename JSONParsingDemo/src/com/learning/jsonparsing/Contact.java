package com.learning.jsonparsing;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class Contact {

	@SerializedName("id")
	private String id;
	
	@SerializedName("name")
	private String name;
	
	@SerializedName("email")
	private String email;
	
	@SerializedName("address")
	private String address;
	
	@SerializedName("gender")
	private String gender;
	
	@SerializedName("phone")
	private Phone phone;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getAddress() {
		return address;
	}

	public String getGender() {
		return gender;
	}

	public Phone getPhone() {
		return phone;
	}

	
	
}
