package com.learning.helloworld;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class ValidationActivity extends Activity implements OnClickListener {

	private EditText etEmail, etPassword;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_validation);

		etEmail = (EditText) findViewById(R.id.et_email);
		etPassword = (EditText) findViewById(R.id.et_password);
		etPassword.setTypeface(Typeface.DEFAULT);
		
		findViewById(R.id.btn_signup).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_signup:
			 final String email = etEmail.getText().toString();
             if (!isValidEmail(email)) {
            	 etEmail.setError("Invalid Email");
             }

             final String pass = etPassword.getText().toString();
             if (!isValidPassword(pass)) {
            	 etPassword.setError("Invalid Password");
             }
			break;

		default:
			break;
		}
	}

	// validating email id
	private boolean isValidEmail(String email) {
		Pattern pattern = Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
				+ "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
				+ "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ "([a-zA-Z0-9]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"); 
//        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

	// validating password with retype password
	private boolean isValidPassword(String pass) {
		if (pass != null && pass.length() > 6) {
			return true;
		}
		return false;
	}
}
