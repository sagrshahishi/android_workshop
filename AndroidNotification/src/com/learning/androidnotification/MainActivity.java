package com.learning.androidnotification;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	private NotificationManager mNotificationManager;
	private int notificationID = 100;
	private int numMessages = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		findViewById(R.id.btn_start).setOnClickListener(this);
		findViewById(R.id.btn_cancel).setOnClickListener(this);
		findViewById(R.id.btn_update).setOnClickListener(this);
		findViewById(R.id.btn_dialog).setOnClickListener(this);
	}

	protected void displayNotification() {
		Log.i("Start", "notification");

		/* Invoking the default notification service */
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this);

		mBuilder.setContentTitle("New Message");
		mBuilder.setContentText("You've received new message.");
		mBuilder.setTicker("New Message Alert!");
		mBuilder.setSmallIcon(R.drawable.io);

		/* Increase notification number every time a new notification arrives */
		mBuilder.setNumber(++numMessages);

		/* Creates an explicit intent for an Activity in your app */
		Intent resultIntent = new Intent(this, NotificationView.class);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addParentStack(NotificationView.class);

		/* Adds the Intent that starts the Activity to the top of the stack */
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);

		mBuilder.setContentIntent(resultPendingIntent);

		// auto cancel
		// mBuilder.setAutoCancel(true);

		// permenant notification
		mBuilder.setOngoing(true);
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		/* notificationID allows you to update the notification later on. */
		mNotificationManager.notify(notificationID, mBuilder.build());

	}

	protected void cancelNotification() {
		Log.i("Cancel", "notification");
		if (mNotificationManager != null) {
			mNotificationManager.cancel(notificationID);
		}

	}

	protected void updateNotification() {
		Log.i("Update", "notification");

		/* Invoking the default notification service */
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this);

		mBuilder.setContentTitle("Updated Message");
		mBuilder.setContentText("You've got updated message.");
		mBuilder.setTicker("Updated Message Alert!");
		mBuilder.setSmallIcon(R.drawable.io);

		/* Increase notification number every time a new notification arrives */
		mBuilder.setNumber(++numMessages);

		/* Creates an explicit intent for an Activity in your app */
		Intent resultIntent = new Intent(this, NotificationView.class);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addParentStack(NotificationView.class);

		/* Adds the Intent that starts the Activity to the top of the stack */
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);

		mBuilder.setContentIntent(resultPendingIntent);

		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		/* Update the existing notification using same notification ID */
		mNotificationManager.notify(notificationID, mBuilder.build());
	}

	protected void showDialog() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				MainActivity.this);

		// Setting Dialog Title
		alertDialog.setTitle("Save File...");

		// Setting Dialog Message
		alertDialog.setMessage("Do you want to save this file?");

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_launcher);

		// Setting Positive Yes Button
		alertDialog.setPositiveButton("YES",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						// User pressed Cancel button. Write Logic Here
						Toast.makeText(getApplicationContext(),
								"You clicked on YES", Toast.LENGTH_SHORT)
								.show();
					}
				});
		// Setting Positive Yes Btn
		alertDialog.setNeutralButton("NO",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						// User pressed No button. Write Logic Here
						Toast.makeText(getApplicationContext(),
								"You clicked on NO", Toast.LENGTH_SHORT).show();
					}
				});
		// Setting Positive "Cancel" Btn
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						// User pressed Cancel button. Write Logic Here
						Toast.makeText(getApplicationContext(),
								"You clicked on Cancel", Toast.LENGTH_SHORT)
								.show();
					}
				});
		// Showing Alert Dialog
		alertDialog.show();
	}

	private void singleChoiceDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setTitle("Show dialog");

		final CharSequence[] choiceList = { "Coke", "Pepsi", "Sprite",
				"Seven Up" };

		int selected = -1; // does not select anything

		builder.setSingleChoiceItems(choiceList, selected,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(MainActivity.this,
								"Selected " + choiceList[which],
								Toast.LENGTH_SHORT).show();
						dialog.dismiss();
					}
				});

		AlertDialog alert = builder.create();
		alert.show();
	}

	private void multipleChoiceDialog() {
		final CharSequence[] digitList = { "One", "Two", "Three" };
		AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
		alt_bld.setIcon(R.drawable.ic_launcher);
		alt_bld.setTitle("Select one Digit");

		alt_bld.setMultiChoiceItems(digitList, new boolean[] { false, true,
				false }, new DialogInterface.OnMultiChoiceClickListener() {
			public void onClick(DialogInterface dialog, int whichButton,
					boolean isChecked) {

			}
		});
		alt_bld.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				ListView list = ((AlertDialog) dialog).getListView();

				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < list.getCount(); i++) {
					boolean checked = list.isItemChecked(i);

					if (checked) {
						if (sb.length() > 0)
							sb.append(", ");
						sb.append(list.getItemAtPosition(i));
					}
				}

				Toast.makeText(getApplicationContext(),
						"Selected digit: " + sb.toString(), Toast.LENGTH_SHORT)
						.show();

			}
		});
		alt_bld.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});

		AlertDialog alert = alt_bld.create();
		alert.show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_start:
			displayNotification();
			break;

		case R.id.btn_cancel:
			cancelNotification();
			break;

		case R.id.btn_update:
			updateNotification();
			break;

		case R.id.btn_dialog:
			 showDialog();
//			singleChoiceDialog();
//			multipleChoiceDialog();
			break;

		default:
			break;
		}
	}

}
