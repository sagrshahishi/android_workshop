AndroidGPSTracking - This project demonstrate the capabilities to track the GPS location and render same on Map.

AndroidGeoFenceing - This project demonstrate capabilities to define the region for fencing and check weather user is within the limit/ cross the limit and coming in limit of fence.

AndroidNotification - This project demonstrate capabilities to show the notification in notification bar.

AndroidPushNotificationsUsingGCM - This project demonstrate push notification capabilities. Though PHP server yet to setup. Independent code is not ready to use because of server dependency.

DetabaseDemo - Storage of application data
— Database
— Files
— Prefrences
— Internal or Removable Storage
— Data backup services


ExternalStorageDemo - Storing persistent data
— private accessible by your application only
— public accessible to other applications.
— how much space your data requires

InternalStorageDemo - Storage Options
— Shared Preferences - store private primitive data in key-value pairs
— Internal Storage - store private data on the device memory.
— External Storage - store public data on the shared external storage.
— SQLite Databases - store structured data in a private database.
— Network Connection - store data on the web site your own network server.

JSONParsingDemo - Way to parse the JSON from any server.

LayoutDemo - Different ways to design layout.

SMSMessaging - Way to send SMS

XMLParsingDemo - Way to parse the XML coming from server.

